#####
# Global variables
####

variable "tags" {
  description = "Tags that will be shared with all the resources of this module"
  default     = {}
}

#####
# S3 bucket
#####

variable "name" {
  description = "Name of the bucket to create."
  type        = string
  default     = ""
}

variable "acl" {
  description = "The canned ACL to apply."
  default     = "private"
}

variable "force_destroy" {
  description = "When set to true, will delete the bucket even if it is not empty."
  default     = false
}

variable "bucket_object_ownership" {
  description = <<EOM
Object ownership. Valid values:
`BucketOwnerPreferred`: Objects uploaded to the bucket change ownership to the bucket owner if the objects are uploaded with the `bucket-owner-full-control` canned ACL.
`ObjectWriter`: The uploading account will own the object if the object is uploaded with the `bucket-owner-full-control` canned ACL.
EOM
  type        = string
  default     = ""

  validation {
    condition = var.bucket_object_ownership == "" || contains([
      "BucketOwnerPreferred", "ObjectWriter"
    ], var.bucket_object_ownership)
    error_message = "The “var.bucket_object_ownership” must be one of “BucketOwnerPreferred” or “ObjectWriter”."
  }
}

variable "bucket_tags" {
  description = "Map of tags that will be added on the bucket object."
  default     = {}
}

variable "request_payment_configuration_payer" {
  description = "Specifies who pays for the download and request fees. Can be either “BucketOwner” or “Requester”."
  default     = "BucketOwner"

  validation {
    condition     = contains(["BucketOwner", "Requester"], var.request_payment_configuration_payer)
    error_message = "“var.request_payment_configuration_payer” must be “BucketOwner” or “Requester”."
  }
}

variable "expected_bucket_owner" {
  description = "The account ID of the expected bucket owner."
  default     = null
  type        = string

  validation {
    condition     = var.expected_bucket_owner == null || can(regex("^[0-9]{12}$", var.expected_bucket_owner))
    error_message = "“var.request_payment_configuration_payer” must match “^[0-9]{12}$”."
  }
}

variable "bucket_policy_enabled" {
  description = "Whether or not to apply the policy `bucket_policy_json` to the bucket."
  default     = false
}

variable "bucket_policy_json" {
  description = "A valid bucket policy JSON document to be applied to the bucket if 'bucket_policy_enabled' is `true`."
  default     = ""
}

variable "sse_enabled" {
  description = "Whether or not to enable server-side encryption. It will use a KMS key created in this module or externally if provided, otherwise, it will use the S3 default encryption"
  type        = bool
  default     = true
}

variable "versioning_configuration" {
  description = <<-DOCUMENTATION
   Configuration for the versioning of the bucket. Once you enable versioning on a bucket, it can never return to an un-versioned state. You can, however, suspend versioning on that bucket
    * status (required, bool):       The versioning state of the bucket. `true` for `Enabled`, `false` for `Suspended`.
    * mfa_delete (optional, bool):   Specifies whether MFA delete is enabled in the bucket versioning configuration. Valid values: `true` for `Enabled` or `false` for `Disabled`.
DOCUMENTATION
  type = object({
    status     = bool
    mfa_delete = optional(bool)
  })
  default = null
}

variable "versioning_mfa" {
  description = "The concatenation of the authentication device's serial number, a space, and the value that is displayed on your authentication device. Required if `mfa_delete` is `Enabled`."
  type        = string
  default     = null
}

variable "website_configuration" {
  description = <<-DOCUMENTATION
   Website configuration for the bucket. See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_website_configuration#index_document for more details.
    * index_document           (optional, object): The name of the index document for the website. Conflicts with `redirect_all_requests_to`.
      * suffix (required, string): A suffix that is appended to a request that is for a directory on the website endpoint. For example, if the suffix is `index.html` and you make a request to `samplebucket/images/`, the data that is returned will be for the object with the key name `images/index.html`. The suffix must not be empty and must not include a slash character.
    * error_document           (optional, object): The name of the error document for the website. Conflicts with `redirect_all_requests_to`.
      * key (required, string): The object key name to use when a 4XX class error occurs.
    * redirect_all_requests_to (optional, object): Granting permissions. See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_logging#target_grant.
      * host_name (required, string): Name of the host where requests are redirected.
      * protocol  (optional, string): Protocol to use when redirecting requests. The default is the protocol that is used in the original request. Valid values: `http`, `https`.
    * routing_rule             (optional, object): List of rules that define when a redirect is applied and the redirect behavior. Conflicts with `redirect_all_requests_to`.
      * condition (optional, object): Condition that must be met for the specified redirect to apply.
        * http_error_code_returned_equals (optional, string): The HTTP error code when the redirect is applied. If specified with key_prefix_equals, then both must be true for the redirect to be applied. Required if `key_prefix_equals` is not specified.
        * key_prefix_equals               (optional, string): The object key name prefix when the redirect is applied. If specified with `http_error_code_returned_equals`, then both must be true for the redirect to be applied. Required if `http_error_code_returned_equals` is not specified.
      * redirect  (required, object): Redirect information.
        * host_name               (optional, string): The host name to use in the redirect request.
        * http_redirect_code      (optional, string): The HTTP redirect code to use on the response.
        * protocol                (optional, string): Protocol to use when redirecting requests. The default is the protocol that is used in the original request. Valid values: `http`, `https`.
        * replace_key_prefix_with (optional, string): The object key prefix to use in the redirect request. For example, to redirect requests for all pages with prefix `docs/` (objects in the `docs/` folder) to `documents/`, you can set a condition block with `key_prefix_equals` set to `docs/` and in the redirect set `replace_key_prefix_with` to `/documents`. Conflicts with `replace_key_with`.
        * replace_key_with        (optional, string): The specific object key to use in the redirect request. For example, redirect request to `error.html`. Conflicts with `replace_key_prefix_with`.
DOCUMENTATION
  type = object({
    index_document = optional(object({
      suffix = string
    }))
    error_document = optional(object({
      key = string
    }))
    redirect_all_requests_to = optional(object({
      host_name = string
      protocol  = optional(string)
    }))
    routing_rule = optional(object({
      condition = optional(object({
        http_error_code_returned_equals = optional(string)
        key_prefix_equals               = optional(string)
      }))
      redirect = optional(object({
        host_name               = optional(string)
        http_redirect_code      = optional(string)
        protocol                = optional(string)
        replace_key_prefix_with = optional(string)
        replace_key_with        = optional(string)
      }))
    }))
  })
  default = null
}

variable "cors_configuration_rules" {
  description = <<-DOCUMENTATION
    List of CORS  configuration rules for the bucket. See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_cors_configuration
    * allowed_headers (optional, list(string)): Set of Headers that are specified in the `Access-Control-Request-Headers` header.
    * allowed_methods (optional, list(string)): Set of HTTP methods that you allow the origin to execute. Valid values are `GET`, `PUT`, `HEAD`, `POST`, and `DELETE`.
    * allowed_origins (optional, list(string)): Set of origins you want customers to be able to access the bucket from.
    * expose_headers  (optional, list(string)): Set of headers in the response that you want customers to be able to access from their applications (for example, from a JavaScript XMLHttpRequest object).
    * id              (optional, string): Unique identifier for the rule. The value cannot be longer than 255 characters.
    * max_age_seconds (optional, number): The time in seconds that your browser is to cache the preflight response for the specified resource.
DOCUMENTATION
  type = list(object({
    allowed_headers = optional(list(string))
    allowed_methods = optional(list(string))
    allowed_origins = optional(list(string))
    expose_headers  = optional(list(string))
    id              = optional(string)
    max_age_seconds = optional(number)
  }))
  default = null
}

variable "lifecycle_configuration_rules" {
  description = <<-DOCUMENTATION
   Lifecycle rules configuration for the bucket. See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_lifecycle_configuration for more details.
    * id                                (required, string): Unique identifier for the rule. The value cannot be longer than 255 characters.
    * status                            (required, string): Whether the rule is currently being applied. Valid values: `Enabled` or `Disabled`.
    * abort_incomplete_multipart_upload (optional, object): Configuration block that specifies the days since the initiation of an incomplete multipart upload that Amazon S3 will wait before permanently removing all parts of the upload.
      * days_after_initiation (required, number): The number of days after which Amazon S3 aborts an incomplete multipart upload.
    * expiration (optional, object): Configuration block that specifies the expiration for the lifecycle of the object in the form of date, days and, whether the object has a delete marker
      * date                         (optional, string): The date the object is to be moved or deleted. Should be in GMT ISO 8601 Format. e.g. `2023-01-13T00:00:00Z`.
      * days                         (optional, number): The lifetime, in days, of the objects that are subject to the rule. The value must be a non-zero positive integer.
      * expired_object_delete_marker (optional, bool): Indicates whether Amazon S3 will remove a delete marker with no noncurrent versions. If set to `true`, the delete marker will be expired; if set to `false` the policy takes no action. Conflicts with `date` and `days`.
    * filter (optional, object): Configuration block used to identify objects that a Lifecycle Rule applies to.
      * and (optional, list(object)): Configuration block used to apply a logical `AND` to two or more predicates. The Lifecycle Rule will apply to any object matching all the predicates configured inside the `and` block.
        * object_size_greater_than (optional, number): Minimum object size to which the rule applies. Value must be at least `0` if specified.
        * object_size_less_than    (optional, number): Maximum object size to which the rule applies. Value must be at least `1` if specified.
        * prefix                   (optional, string): Prefix identifying one or more objects to which the rule applies.
        * tag                      (optional, map(string)): Key-value map of resource tags. All of these tags must exist in the object's tag set in order for the rule to apply.
    * noncurrent_version_expiration (optional, object): Configuration block that specifies when noncurrent object versions expire.
      * newer_noncurrent_versions (optional, number): The number of noncurrent versions Amazon S3 will retain.
      * noncurrent_days           (optional, number): The number of days an object is noncurrent before Amazon S3 can perform the associated action. Must be a positive integer.
    * noncurrent_version_transition (optional, object): Set of configuration blocks that specify the transition rule for the lifecycle rule that describes when noncurrent objects transition to a specific storage class
      * newer_noncurrent_versions (optional, number): The number of noncurrent versions Amazon S3 will retain.
      * noncurrent_days           (optional, number): The number of days an object is noncurrent before Amazon S3 can perform the associated action. Must be a positive integer.
      * storage_class             (required, string): The class of storage used to store the object. Valid Values: `GLACIER`, `STANDARD_IA`, `ONEZONE_IA`, `INTELLIGENT_TIERING`, `DEEP_ARCHIVE`, `GLACIER_IR`.
    * transition (optional, object): Set of configuration blocks that specify the transition rule for the lifecycle rule that describes when noncurrent objects transition to a specific storage class
      * date          (optional, string): The date objects are transitioned to the specified storage class. The date value must be in ISO 8601 format and set to midnight UTC e.g. `2023-01-13T00:00:00Z`.
      * days          (optional, number): The number of days after creation when objects are transitioned to the specified storage class. The value must be a positive integer. If both `days` and `date` are not specified, defaults to `0`. Valid values depend on `storage_class`.
      * storage_class (required, string): The class of storage used to store the object. Valid Values: `GLACIER`, `STANDARD_IA`, `ONEZONE_IA`, `INTELLIGENT_TIERING`, `DEEP_ARCHIVE`, `GLACIER_IR`.
DOCUMENTATION

  type = list(object({
    id     = string
    status = string
    abort_incomplete_multipart_upload = optional(object({
      days_after_initiation = number
    }))
    expiration = optional(object({
      date                         = optional(string)
      days                         = optional(number)
      expired_object_delete_marker = optional(bool)
    }))
    filter = optional(object({
      and = optional(map(object({
        object_size_greater_than = optional(number)
        object_size_less_than    = optional(number)
        prefix                   = optional(string)
        tags                     = optional(map(string))
      })))
    }))
    noncurrent_version_expiration = optional(object({
      newer_noncurrent_versions = optional(number)
      noncurrent_days           = optional(number)
    }))
    noncurrent_version_transition = optional(object({
      newer_noncurrent_versions = optional(number)
      noncurrent_days           = optional(number)
      storage_class             = string
    }))
    transition = optional(object({
      date          = optional(string)
      days          = optional(number)
      storage_class = string
    }))
  }))
  default = null
}

variable "logging_configuration" {
  description = <<-DOCUMENTATION
   Configure logging on bucket object. It is highly recommended to use logging for good security practice.
    * target_bucket (required, string): The bucket where you want Amazon S3 to store server access logs.
    * target_prefix (required, string): A prefix for all log object keys.
    * target_grant  (optional, object): Granting permissions. See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_logging#target_grant.
DOCUMENTATION
  type = object({
    target_bucket = string
    target_prefix = string
    target_grant = optional(object({
      grantee = object({
        email_address = optional(string)
        id            = optional(string)
        type          = string
        uri           = optional(string)
      })
      permission = string
    }))
  })
  default = null
}

variable "object_lock_configuration" {
  description = <<-DOCUMENTATION
    Configure an object lock configuration on the bucket objects. See https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object_lock_configuration.
    * rule (required, object): The bucket where you want Amazon S3 to store server access logs.
      * default_retention (required, object): The bucket where you want Amazon S3 to store server access logs.
        * mode  (required, string): The default Object Lock retention mode you want to apply to new objects placed in the specified bucket. Valid values: `COMPLIANCE`, `GOVERNANCE`.
        * days  (optional, number): The number of days that you want to specify for the default retention period. Required if `years` is not specified.
        * years (optional, number): The number of years that you want to specify for the default retention period. Required if `days` is not specified.
DOCUMENTATION
  type = object({
    rule = object({
      default_retention = object({
        mode  = string
        days  = optional(number)
        years = optional(number)
      })
    })
  })

  default = null
}

variable "block_public_acls" {
  description = "Whether Amazon S3 should block public ACLs for this bucket."
  type        = bool
  default     = false
}

variable "block_public_policy" {
  description = "Whether Amazon S3 should block public bucket policies for this bucket."
  type        = bool
  default     = false
}

variable "ignore_public_acls" {
  description = "Whether Amazon S3 should ignore public ACLs for this bucket."
  type        = bool
  default     = false
}

variable "restrict_public_buckets" {
  description = "Whether Amazon S3 should restrict public bucket policies for this bucket."
  type        = bool
  default     = false
}

#####
# Route53
#####

variable "route53_records" {
  description = <<DOCUMENTATION
Map of records to link the the S3 bucket created in this module. If empty, no domain name alias will be created.
name (string)        : the CNAME record value (without the TLD domain name) to be used as an alias for the S3 bucket domain name.
zone_id (string)     : the ID of the zone administrating the domain.
ttl optional(number) : the TTL of the CNAME record. Defaults to 300.
DOCUMENTATION
  default     = {}
  type = map(object({
    name    = string,
    zone_id = string,
    ttl     = optional(number),
  }))

  validation {
    condition = var.route53_records == {} || (
      (
        !contains([
          for k, content in var.route53_records :
          (
            can(regex("^Z([A-Z0-9]{20}|[A-Z0-9]{13})", content.zone_id)) &&
            (
              defaults(content, { ttl = 1 }).ttl <= 65535 || defaults(content, { ttl = 1 }).ttl >= 0
            )
          )
        ], false)
      )
    )
    error_message = "One or more “var.route53_records” are invalid. Check the requirements in the variables.tf file."
  }
}

#####
# KMS key
#####

variable "kms_key_arn" {
  description = "ARN of an external KMS key to use (or better, a KMS key alias). Ignored if toggle `var.kms_key_create` is `true`. If set and `var.sse_config` is empty, the created KMS key will be used for the bucket encryption."
  type        = string
  default     = ""
}

variable "kms_key_create" {
  description = "Create a KMS key for the bucket. If `true` and `var.sse_config` is empty, the created KMS key will be used for the bucket encryption."
  default     = false
}

variable "kms_key_name" {
  description = "Name of the KMS key to create if toggle `var.kms_key_create` is `true`."
  default     = ""
}

variable "kms_key_rotation_enabled" {
  description = "Choose whether key rotation is enabled. It is highly recommended to keep this value to true, for good security practice."
  type        = bool
  default     = true
}

variable "kms_tags" {
  description = "Tags specific to the KMS key to create if toggle `var.kms_key_create` is `true`. Will be merged `var.tags`."
  default     = {}
}

variable "kms_key_alias_name" {
  description = "Alias of the KMS key to create if toggle `var.kms_key_create` is `true`."
  default     = ""
}

variable "kms_key_policy_json" {
  description = "Policy (in JSON) to attach to the KMS key to create if toggle `var.kms_key_create` is `true`."
  default     = ""
}

#####
# Service IAM User
#####

variable "iam_user_enabled" {
  description = "Whether or not to create a service IAM user to access the S3 data created within this module. Careful, this user give *data access* according to `var.iam_user_data_access_permission`, not AWS resource access. Note that it will enabled IAM policies if you toggle that value."
  type        = bool
  default     = false
}

variable "iam_user_access_key_enabled" {
  description = "Whether or not to create the access key for the service IAM user to access the S3 data created within this module. Careful, enabling this will store a secret key unencrypted within Terraform state file."
  type        = bool
  default     = false
}

variable "iam_user_data_access_permission" {
  description = "Permission granted for the data server IAM user over the S3 bucket created by this module. Either “RO” or “RW”. Note that it will give data access, not AWS resources access. Ignored if `var.iam_user_enabled` is `false`."
  type        = string
  default     = "RO"

  validation {
    condition     = contains(["RW", "RO"], var.iam_user_data_access_permission)
    error_message = "“var.iam_user_data_access_permission” must be “RO” or “RW”."
  }
}

variable "iam_user_name" {
  description = "Name of the service IAM user to access the S3 data created within this module. Ignored if `var.iam_user_enabled` is `false`."
  type        = string
  default     = ""

  validation {
    condition     = var.iam_user_name == "" || can(regex("^[0-9a-zA-Z_+=,.@-]+$", var.iam_user_name))
    error_message = "“var.iam_user_name” must match '^[0-9a-zA-Z_+=,.@-]+$'."
  }
}

variable "iam_user_path" {
  description = "Path for the service IAM user to access the S3 data created within this module. Ignored if `var.iam_user_enabled` is `false`."
  type        = string
  default     = "/"

  validation {
    condition     = var.iam_user_path == "/" || can(regex("^/([0-9a-z]+/)+$", var.iam_user_path))
    error_message = "“var.iam_user_path” must match '^/([0-9a-z]+/)+$'."
  }
}

variable "iam_user_tags" {
  description = "Tags to be used for the service IAM user to access the S3 data created within this module. Will be merged with `var.tags`. Ignored if `var.iam_user_enabled` is `false`."
  default     = {}
}

####
# IAM Policy
#####

variable "iam_policy_create" {
  description = "Whether or not to create IAM policies for resources of this module. If `var.iam_user_enabled` is `true`, policies will be created anyway, ignoring this value."
  default     = false
}

variable "iam_policy_output_jsons" {
  description = "Whether or not to output IAM policy JSONs for resources of this module."
  default     = true
}

variable "iam_policy_read_name" {
  description = "Name of the IAM read only access to S3 bucket."
  default     = ""
}

variable "iam_policy_full_name" {
  description = "Name of the IAM read/write access to S3 bucket."
  default     = ""
}

variable "iam_policy_data_rw_name" {
  description = "Name of the IAM ReadWrite objects access to S3 bucket."
  default     = ""
}

variable "iam_policy_data_ro_name" {
  description = "Name of the IAM ReadOnly objects access to S3 bucket."
  default     = ""
}

variable "iam_policy_path" {
  description = "Path in which to create the policies."
  default     = "/"
}

variable "iam_policy_tags" {
  description = "Tags specific to the IAM policies to create if toggle `var.iam_policy_create` is `true`. Will be merged `var.tags`."
  default     = {}
}
