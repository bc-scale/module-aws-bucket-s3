data "aws_caller_identity" "current" {}

#####
# With kms and bucket policy
#####

data "aws_iam_policy_document" "policy" {
  statement {
    sid = "1"

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
      ]
    }

    actions = [
      "s3:*",
    ]
    resources = [
      module.policy.arn,
    ]
  }
}

data "aws_iam_policy_document" "policy_no_rotation" {
  statement {
    sid = "1"

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
      ]
    }

    actions = [
      "s3:*",
    ]
    resources = [
      module.policy_no_kms_rotation.arn,
    ]
  }
}

data "aws_iam_policy_document" "kms" {
  statement {
    sid = "1"

    effect = "Allow"

    principals {
      type = "AWS"

      identifiers = [
        replace(replace(join("/", chunklist(split("/", data.aws_caller_identity.current.arn), 2)[0]), "sts", "iam"), "assumed-role", "role")
      ]
    }

    actions = [
      "kms:*",
    ]

    resources = [
      "*",
    ]
  }
}
