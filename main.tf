locals {
  tags = merge(
    {
      managed-by = "terraform"
      origin     = "gitlab.com/wild-beavers/terraform/module-aws-bucket-s3"
    },
    var.tags
  )

  sse_config = {
    sse_algorithm     = local.use_kms_key ? "aws:kms" : "AES256"
    kms_master_key_id = local.use_kms_key ? local.kms_key_arn : null
  }
}

#####
# S3 bucket
#####

resource "aws_s3_bucket" "this" {
  bucket = var.name

  force_destroy = var.force_destroy

  object_lock_enabled = var.object_lock_configuration != null

  tags = merge(
    {
      "Name" = var.name
    },
    local.tags,
    var.bucket_tags,
  )
}

resource "aws_s3_bucket_policy" "this" {
  count = var.bucket_policy_enabled == true ? 1 : 0

  bucket = aws_s3_bucket.this.id
  policy = var.bucket_policy_json
}

resource "aws_s3_bucket_ownership_controls" "this" {
  count = var.bucket_object_ownership != "" ? 1 : 0

  bucket = aws_s3_bucket.this.id

  rule {
    object_ownership = var.bucket_object_ownership
  }
}

resource "aws_s3_bucket_object_lock_configuration" "this" {
  count = var.object_lock_configuration != null ? 1 : 0

  bucket                = aws_s3_bucket.this.id
  expected_bucket_owner = var.expected_bucket_owner

  rule {
    default_retention {
      mode  = var.object_lock_configuration.rule.default_retention.mode
      days  = lookup(var.object_lock_configuration.rule.default_retention, "days", null)
      years = lookup(var.object_lock_configuration.rule.default_retention, "years", null)
    }
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  count = 1

  bucket = aws_s3_bucket.this.id

  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets

  depends_on = [aws_s3_bucket_policy.this]
}

resource "aws_s3_bucket_request_payment_configuration" "this" {
  count = 1

  bucket                = aws_s3_bucket.this.bucket
  expected_bucket_owner = var.expected_bucket_owner
  payer                 = var.request_payment_configuration_payer
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  count = var.sse_enabled ? 1 : 0

  bucket                = aws_s3_bucket.this.bucket
  expected_bucket_owner = var.expected_bucket_owner

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = lookup(local.sse_config, "sse_algorithm", null)
      kms_master_key_id = lookup(local.sse_config, "kms_master_key_id", null)
    }
  }
}

resource "aws_s3_bucket_acl" "this" {
  count = var.acl != "private" ? 1 : 0

  bucket                = aws_s3_bucket.this.id
  expected_bucket_owner = var.expected_bucket_owner

  acl = var.acl
}

resource "aws_s3_bucket_versioning" "this" {
  count = var.versioning_configuration != null ? 1 : 0

  bucket                = aws_s3_bucket.this.id
  expected_bucket_owner = var.expected_bucket_owner

  mfa = var.versioning_mfa

  versioning_configuration {
    status     = var.versioning_configuration.status ? "Enabled" : "Suspended"
    mfa_delete = lookup(var.versioning_configuration, "mfa_delete", null) != null ? (lookup(var.versioning_configuration, "mfa_delete", null) ? "Enabled" : "Disabled") : null
  }
}

resource "aws_s3_bucket_logging" "this" {
  count = var.logging_configuration != null ? 1 : 0

  bucket                = aws_s3_bucket.this.id
  expected_bucket_owner = var.expected_bucket_owner

  target_bucket = var.logging_configuration.target_bucket
  target_prefix = var.logging_configuration.target_prefix

  dynamic "target_grant" {
    for_each = lookup(var.logging_configuration, "target_grant", null) != null ? { 0 = lookup(var.logging_configuration, "target_grant", {}) } : {}

    content {
      grantee {
        email_address = lookup(target_grant.value.grantee, "email_address", null)
        id            = lookup(target_grant.value.grantee, "id", null)
        type          = target_grant.value.type
        uri           = lookup(target_grant.value.grantee, "uri", null)
      }

      permission = lookup(var.logging_configuration.target_grant, "permission", null)
    }
  }
}

resource "aws_s3_bucket_website_configuration" "this" {
  count = var.website_configuration != null ? 1 : 0

  bucket                = aws_s3_bucket.this.id
  expected_bucket_owner = var.expected_bucket_owner

  dynamic "index_document" {
    for_each = lookup(var.website_configuration, "index_document", null) != null ? { 0 = lookup(var.website_configuration, "index_document", {}) } : {}

    content {
      suffix = index_document.value.suffix
    }
  }

  dynamic "error_document" {
    for_each = lookup(var.website_configuration, "error_document", null) != null ? { 0 = lookup(var.website_configuration, "error_document", {}) } : {}

    content {
      key = error_document.value.key
    }
  }

  dynamic "redirect_all_requests_to" {
    for_each = lookup(var.website_configuration, "redirect_all_requests_to", null) != null ? { 0 = lookup(var.website_configuration, "redirect_all_requests_to", {}) } : {}

    content {
      host_name = redirect_all_requests_to.value.host_name
      protocol  = lookup(redirect_all_requests_to.value, "protocol", null)
    }
  }

  dynamic "routing_rule" {
    for_each = lookup(var.website_configuration, "routing_rule", null) != null ? { 0 = lookup(var.website_configuration, "routing_rule", {}) } : {}

    content {
      dynamic "condition" {
        for_each = lookup(routing_rule.value, "condition", null) != null ? { 0 = lookup(routing_rule.value, "condition", {}) } : {}

        content {
          http_error_code_returned_equals = lookup(condition.value, "http_error_code_returned_equals", null)
          key_prefix_equals               = lookup(condition.value, "key_prefix_equals", null)
        }
      }

      redirect {
        host_name               = lookup(routing_rule.value.redirect, "host_name", null)
        http_redirect_code      = lookup(routing_rule.value.redirect, "http_redirect_code", null)
        protocol                = lookup(routing_rule.value.redirect, "protocol", null)
        replace_key_prefix_with = lookup(routing_rule.value.redirect, "replace_key_prefix_with", null)
        replace_key_with        = lookup(routing_rule.value.redirect, "replace_key_with", null)
      }
    }
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "this" {
  count = var.lifecycle_configuration_rules != null ? 1 : 0

  bucket                = aws_s3_bucket.this.id
  expected_bucket_owner = var.expected_bucket_owner

  dynamic "rule" {
    for_each = var.lifecycle_configuration_rules

    content {
      id     = rule.value.id
      status = rule.value.status

      dynamic "expiration" {
        for_each = lookup(rule.value, "expiration", null) != null ? { 0 = lookup(rule.value, "expiration", {}) } : {}

        content {
          date                         = lookup(expiration.value, "date", null)
          days                         = lookup(expiration.value, "days", null)
          expired_object_delete_marker = lookup(expiration.value, "expired_object_delete_marker", null)
        }
      }

      dynamic "filter" {
        for_each = lookup(rule.value, "filter", null) != null ? { 0 = lookup(rule.value, "filter", {}) } : {}

        content {
          object_size_greater_than = lookup(filter.value, "object_size_greater_than", null)
          object_size_less_than    = lookup(filter.value, "object_size_less_than", null)
          prefix                   = lookup(filter.value, "prefix", null)

          dynamic "and" {
            for_each = lookup(filter.value, "and", null) != null ? lookup(filter.value, "and", {}) : {}

            content {
              object_size_greater_than = lookup(and.value, "object_size_greater_than", null)
              object_size_less_than    = lookup(and.value, "object_size_less_than", null)
              prefix                   = lookup(and.value, "prefix", null)
              tags                     = lookup(and.value, "tags", null)
            }
          }

          dynamic "tag" {
            for_each = lookup(filter.value, "tag", null) != null ? { 0 = lookup(filter.value, "tag", {}) } : {}

            content {
              key   = tag.value.key
              value = tag.value.value
            }
          }
        }
      }

      dynamic "noncurrent_version_expiration" {
        for_each = lookup(rule.value, "noncurrent_version_expiration", null) != null ? { 0 = lookup(rule.value, "noncurrent_version_expiration", {}) } : {}

        content {
          newer_noncurrent_versions = lookup(noncurrent_version_expiration.value, "newer_noncurrent_versions", null)
          noncurrent_days           = lookup(noncurrent_version_expiration.value, "noncurrent_days", null)
        }
      }

      dynamic "noncurrent_version_transition" {
        for_each = lookup(rule.value, "noncurrent_version_transition", null) != null ? { 0 = lookup(rule.value, "noncurrent_version_transition", {}) } : {}

        content {
          newer_noncurrent_versions = lookup(noncurrent_version_transition.value, "newer_noncurrent_versions", null)
          noncurrent_days           = lookup(noncurrent_version_transition.value, "noncurrent_days", null)
          storage_class             = noncurrent_version_transition.value.storage_class
        }
      }

      dynamic "transition" {
        for_each = lookup(rule.value, "transition", null) != null ? { 0 = lookup(rule.value, "transition", {}) } : {}

        content {
          date          = lookup(transition.value, "date", null)
          days          = lookup(transition.value, "days", null)
          storage_class = transition.value.storage_class
        }
      }
    }
  }
}

resource "aws_s3_bucket_cors_configuration" "this" {
  count = var.cors_configuration_rules != null ? 1 : 0

  bucket                = aws_s3_bucket.this.id
  expected_bucket_owner = var.expected_bucket_owner

  dynamic "cors_rule" {
    for_each = var.cors_configuration_rules

    content {
      allowed_headers = lookup(cors_rule.value, "allowed_headers", null)
      allowed_methods = lookup(cors_rule.value, "allowed_methods", null)
      allowed_origins = lookup(cors_rule.value, "allowed_origins", null)
      expose_headers  = lookup(cors_rule.value, "expose_headers", null)
      max_age_seconds = lookup(cors_rule.value, "max_age_seconds", null)
      id              = lookup(cors_rule.value, "id", null)
    }
  }
}

#####
# KMS
#####

locals {
  use_kms_key = var.kms_key_create || var.kms_key_arn != ""
  kms_key_arn = var.kms_key_create ? aws_kms_alias.this.*.arn[0] : var.kms_key_arn
}

resource "aws_kms_key" "this" {
  count = var.kms_key_create ? 1 : 0

  description = "KMS Key for ${var.name} S3 encryption."
  policy      = var.kms_key_policy_json

  enable_key_rotation = var.kms_key_rotation_enabled

  tags = merge(
    {
      "Name" = var.kms_key_name
    },
    local.tags,
    var.kms_tags,
  )
}

resource "aws_kms_alias" "this" {
  count = var.kms_key_create ? 1 : 0

  name          = "alias/${var.kms_key_alias_name}"
  target_key_id = element(concat(aws_kms_key.this.*.key_id, [""]), 0)
}

#####
# Service IAM User
#####

resource "aws_iam_user" "data" {
  count = var.iam_user_enabled ? 1 : 0

  name = var.iam_user_name
  path = var.iam_user_path

  tags = merge(
    {
      Name        = var.iam_user_name
      Description = "Service user to access the data of the ${var.name} S3 bucket."
    },
    local.tags,
    var.iam_user_tags
  )
}

resource "aws_iam_access_key" "data" {
  count = var.iam_user_enabled && var.iam_user_access_key_enabled ? 1 : 0

  user = aws_iam_user.data.*.name[0]
}

resource "aws_iam_user_policy_attachment" "data" {
  count = var.iam_user_enabled ? 1 : 0

  user       = aws_iam_user.data.*.name[0]
  policy_arn = var.iam_user_data_access_permission == "RW" ? aws_iam_policy.this_objects_rw.*.arn[0] : aws_iam_policy.this_objects_ro.*.arn[0]
}

#####
# Route53
#####

resource "aws_route53_record" "this" {
  for_each = var.route53_records

  allow_overwrite = true
  name            = each.value.name
  records         = [aws_s3_bucket.this.bucket_regional_domain_name]
  ttl             = defaults(each.value, { ttl = 300 }).ttl
  type            = "CNAME"
  zone_id         = each.value.zone_id
}

#####
# IAM policy
#####

resource "aws_iam_policy" "this_read" {
  count = var.iam_policy_create ? 1 : 0

  name   = var.iam_policy_read_name
  path   = var.iam_policy_path
  policy = data.aws_iam_policy_document.this_read[0].json

  description = "Grant read only access to data and direct AWS resource access for the ${var.name} bucket."

  tags = merge(
    {
      "Description" = "Grant read only access to data and direct AWS resource access for the ${var.name} bucket."
      "Name"        = var.iam_policy_read_name
    },
    local.tags,
    var.iam_policy_tags,
  )
}

resource "aws_iam_policy" "this_full" {
  count = var.iam_policy_create ? 1 : 0

  name   = var.iam_policy_full_name
  path   = var.iam_policy_path
  policy = data.aws_iam_policy_document.this_full[0].json

  description = "Grant full read and write access to data and direct AWS resource access for the ${var.name} bucket."

  tags = merge(
    {
      "Description" = "Grant full read and write access to data and direct AWS resource access for the ${var.name} bucket."
      "Name"        = var.iam_policy_full_name
    },
    local.tags,
    var.iam_policy_tags,
  )
}

resource "aws_iam_policy" "this_objects_rw" {
  count = var.iam_policy_create || (var.iam_user_enabled && var.iam_user_data_access_permission == "RW") ? 1 : 0

  name   = var.iam_policy_data_rw_name
  path   = var.iam_policy_path
  policy = data.aws_iam_policy_document.this_data_rw[0].json

  description = "Grant read and write access to the data of the ${var.name} bucket."

  tags = merge(
    {
      "Description" = "Grant read and write access to the data of the ${var.name} bucket."
      "Name"        = var.iam_policy_data_rw_name
    },
    local.tags,
    var.iam_policy_tags,
  )
}

resource "aws_iam_policy" "this_objects_ro" {
  count = var.iam_policy_create || (var.iam_user_enabled && var.iam_user_data_access_permission == "RO") ? 1 : 0

  name   = var.iam_policy_data_ro_name
  path   = var.iam_policy_path
  policy = data.aws_iam_policy_document.this_data_ro[0].json

  description = "Grant read only access to the data of the ${var.name} bucket."

  tags = merge(
    {
      "Description" = "Grant read only access to the data of the ${var.name} bucket."
      "Name"        = var.iam_policy_data_ro_name
    },
    local.tags,
    var.iam_policy_tags,
  )
}
