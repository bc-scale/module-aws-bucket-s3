#####
# S3 bucket
#####

output "standard_id" {
  value = module.standard.id
}

output "standard_arn" {
  value = module.standard.arn
}

output "standard_bucket_domain_name" {
  value = module.standard.bucket_domain_name
}

output "standard_bucket_regional_domain_name" {
  value = module.standard.bucket_regional_domain_name
}

output "standard_hosted_zone_id" {
  value = module.standard.hosted_zone_id
}

output "standard_region" {
  value = module.standard.region
}

#####
# KMS key
#####

output "standard_kms_key_arn" {
  value = module.standard.kms_key_arn
}

output "standard_kms_key_id" {
  value = module.standard.kms_key_id
}

output "standard_kms_alias_arn" {
  value = module.standard.kms_alias_arn
}

output "standard_kms_alias_target_key_arn" {
  value = module.standard.kms_alias_target_key_arn
}

#####
# Service IAM User
#####

output "standard_iam_user_arn" {
  value = module.standard.iam_user_arn
}

output "standard_iam_user_name" {
  value = module.standard.iam_user_name
}

output "standard_iam_user_unique_id" {
  value = module.standard.iam_user_unique_id
}

output "standard_iam_user_iam_access_key_id" {
  value     = module.standard.iam_user_iam_access_key_id
  sensitive = true
}

output "standard_iam_user_iam_access_key_secret" {
  value     = module.standard.iam_user_iam_access_key_secret
  sensitive = true
}

#####
# IAM policy
#####

output "standard_iam_policy_read_only_id" {
  value = module.standard.iam_policy_read_only_id
}

output "standard_iam_policy_read_only_arn" {
  value = module.standard.iam_policy_read_only_arn
}

output "standard_iam_policy_read_only_description" {
  value = module.standard.iam_policy_read_only_description
}

output "standard_iam_policy_read_only_name" {
  value = module.standard.iam_policy_read_only_name
}

output "standard_iam_policy_read_only_json" {
  value = module.standard.iam_policy_read_only_json
}

output "standard_iam_policy_full_id" {
  value = module.standard.iam_policy_full_id
}

output "standard_iam_policy_full_arn" {
  value = module.standard.iam_policy_full_arn
}

output "standard_iam_policy_full_description" {
  value = module.standard.iam_policy_full_description
}

output "standard_iam_policy_full_name" {
  value = module.standard.iam_policy_full_name
}

output "standard_iam_policy_full_json" {
  value = module.standard.iam_policy_full_json
}

output "standard_iam_policy_data_ro_id" {
  value = module.standard.iam_policy_data_ro_id
}

output "standard_iam_policy_data_ro_arn" {
  value = module.standard.iam_policy_data_ro_arn
}

output "standard_iam_policy_data_ro_description" {
  value = module.standard.iam_policy_data_ro_description
}

output "standard_iam_policy_data_ro_name" {
  value = module.standard.iam_policy_data_ro_name
}

output "standard_iam_policy_data_ro_json" {
  value = module.standard.iam_policy_data_ro_json
}

output "standard_iam_policy_data_rw_id" {
  value = module.standard.iam_policy_data_rw_id
}

output "standard_iam_policy_data_rw_arn" {
  value = module.standard.iam_policy_data_rw_arn
}

output "standard_iam_policy_data_rw_description" {
  value = module.standard.iam_policy_data_rw_description
}

output "standard_iam_policy_data_rw_name" {
  value = module.standard.iam_policy_data_rw_name
}

output "standard_iam_policy_data_rw_json" {
  value = module.standard.iam_policy_data_rw_json
}

output "standard_iam_policy_kms_ro_json" {
  value = module.standard.iam_policy_kms_ro_json
}

output "standard_iam_policy_kms_data_ro_json" {
  value = module.standard.iam_policy_kms_data_ro_json
}

output "standard_iam_policy_kms_rw_json" {
  value = module.standard.iam_policy_kms_rw_json
}

output "standard_iam_policy_kms_data_rw_json" {
  value = module.standard.iam_policy_kms_data_rw_json
}
