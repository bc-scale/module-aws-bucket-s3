module "static_website" {
  source = "../../"

  acl  = "public-read"
  name = "tftestwebsite${random_string.this.result}"

  kms_key_create          = true
  kms_key_name            = "tftestwebsite${random_string.this.result}"
  kms_key_alias_name      = "tftestwebsite${random_string.this.result}"
  iam_policy_create       = true
  iam_policy_read_name    = "tftestwebsiteread${random_string.this.result}"
  iam_policy_full_name    = "tftestwebsitefull${random_string.this.result}"
  iam_policy_data_ro_name = "tftestwebsitero${random_string.this.result}"
  iam_policy_data_rw_name = "tftestwebsiterw${random_string.this.result}"

  website_configuration = {
    index_document = {
      suffix = "index.html"
    }
    error_document = {
      key = "error.html"
    }
    routing_rule = {
      condition = {
        key_prefix_equals = "docs/"
      }

      redirect = {
        replace_key_prefix_with = "documents/"
      }
    }
  }

  cors_configuration_rules = [
    {
      id              = "tftest1"
      allowed_headers = ["*"]
      allowed_methods = ["PUT", "POST"]
      allowed_origins = ["https://s3-website-test.hashicorp.com"]
      expose_headers  = ["ETag"]
      max_age_seconds = 3000
    },
    {
      allowed_methods = ["GET"]
      allowed_origins = ["*"]
    }
  ]
}
